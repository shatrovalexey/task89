=end
Задача №2
Имеется строка:
https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3
Напишите функцию, которая:
1. удалит параметры со значением “3”;
2. отсортирует параметры по значению;
3. добавит параметр url со значением из переданной
ссылки без параметров (в примере: /test/index.html);
4. сформирует и вернёт валидный URL на корень
указанного в ссылке хоста.
В указанном примере функцией должно быть
возвращено:
https://www.somehost.com/?param4=1&param3=2&param1=4&url=%2Ftest%2Findex.html
=cut

use strict ;
use warnings ;
use locale ;
use utf8 ;

use URI ;
use URI::QueryParam ;
use URI::WithBase ;

=end
парсинг URI
=cut
sub uri_parse( $ ) { URI->new( @_ ) }

=end
фильтр HTTP-аргументов URI
=cut
sub uri_filter_param( &$ ) {
	my ( $sub , $uri ) = @_ ;
	my $urih = uri_parse $uri ;
	my %args = $urih->query_form( ) ;

	$urih->query_form( $sub->( \%args ) ) ;

	$urih
}

=end
1. удалит параметры со значением “3”;
=cut
sub uri_delete_param_by_value( $$ ) {
	my ( $uri , $value ) = @_ ;

	uri_filter_param { +
		my ( $args ) = @_ ;

		map { +
			$_ => $args->{ $_ }
		} grep { +
			ref or $args->{ $_ } ne $value
		} keys %$args
	} $uri
}

=end
2. отсортирует параметры по значению;
=cut
sub uri_order_param_by_value( $ ) {
	my ( $uri ) = @_ ;

	uri_filter_param { +
		my ( $args ) = @_ ;

		map { +
			$_ => $args->{ $_ }
		} sort { +
			$args->{ $b } <=> $args->{ $a }
		} keys %$args
	} $uri
}

=end
3. добавит параметр url со значением из переданной ссылки без параметров (в примере: /test/index.html);
=cut
sub uri_add_param_key_value( $$$ ) {
	my ( $uri , $key , $value ) = @_ ;

	my $urih = uri_filter_param {
		my ( $args ) = @_ ;

		%$args , $key => $value
	} $uri ;
}

=end
4. сформирует и вернёт валидный URL на корень указанного в ссылке хоста.
=cut
sub uri_base_path( $ ) {
	my ( $uri ) = @_ ;
	my $urih = uri_parse $uri ;

	$urih->path( '/' ) ;
	$urih->fragment( undef ) ;
	$urih->query( undef ) ;

	$urih
}

=end
Тестирование функций (хотя можно было бы организовать в пакет)
=cut

my $input_fh = \*DATA ; # входные поток с URI
my $pos = tell $input_fh ; # начальная позиция курсора входного потока

local ( $, , $\ ) = ( "\t" , "\n" ) ; # настройка вывода

foreach my $test ( # тестирование функций из задачи
	sort { +
		$a->[ 0 ] cmp $b->[ 0 ]
	}
	[ '2. отсортирует параметры по значению;' => \&uri_order_param_by_value , ] ,
	[ '1. удалит параметры со значением “3”' => sub( $ ) {
		my ( $uri ) = @_ ;

		uri_delete_param_by_value $uri , '3'
	} ] ,
	[ '3. добавит параметр url со значением из переданной ссылки без параметров (в примере: /test/index.html);' => sub( $ ) {
		my ( $uri ) = @_ ;

		uri_add_param_key_value $uri , 'key' => 'value'
	} ] ,
	[ '4. сформирует и вернёт валидный URL на корень указанного в ссылке хоста.' => \&uri_base_path , ]
) {
	my ( $title , $sub ) = @$test ;

	print $title ;

	m{\S+}os and print $sub->( $& ) while <$input_fh> ;

	print ;

	seek $input_fh , $pos , 0
}

__END__
https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3