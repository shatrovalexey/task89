/**
Имеется база со следующими таблицами:
CREATE TABLE `users` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`name` VARCHAR(255) DEFAULT NULL,
`gender` INT(11) NOT NULL COMMENT '0 - не указан, 1 - мужчина, 2 -
женщина.',
`birth_date` INT(11) NOT NULL COMMENT 'Дата в unixtime.',
PRIMARY KEY (`id`)
);
CREATE TABLE `phone_numbers` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`user_id` INT(11) NOT NULL,
`phone` VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (`id`)
);
Напишите запрос, возвращающий имя и число
указанных телефонных номеров девушек в возрасте
от 18 до 22 лет.
Оптимизируйте таблицы и запрос при необходимости.
*/

CREATE TABLE `users` (
	`id` INT( 11 ) NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`name` VARCHAR( 255 ) DEFAULT null COMMENT 'название' ,
	`gender` INT( 11 ) NOT null COMMENT '0 - не указан, 1 - мужчина, 2 - женщина.' ,
	`birth_date` INT( 11 ) NOT NULL COMMENT 'Дата в unixtime.',
	PRIMARY KEY (`id`)
);
CREATE TABLE `phone_numbers` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`user_id` INT(11) NOT NULL,
`phone` VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (`id`)
);

/**
* Напишите запрос, возвращающий имя и число
* указанных телефонных номеров девушек в возрасте
* от 18 до 22 лет.
* Оптимизируйте таблицы и запрос при необходимости.
*/
ALTER TABLE `users`
COMMENT = 'пользователь' ,
CHANGE `id` `id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
CHANGE `gender` `gender` TINYINT( 1 ) UNSIGNED NOT null DEFAULT 0 COMMENT '0 - не указан, 1 - мужчина, 2 - женщина, 3 - девушка.' ;

ALTER TABLE `phone_numbers`
COMMENT = 'номер телефона' ,
CHANGE `id` `id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
CHANGE `user_id` `users_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор пользователя' ,
ADD FOREIGN KEY( `users_id` ) REFERENCES `users`( `id` ) ON UPDATE CASCADE ON DELETE CASCADE ;

-- оставить только последние записи пользователей с номерами телефонов
DELETE
	`pn1`.*
FROM
	`phone_numbers` AS `pn1`

	LEFT OUTER JOIN `phone_numbers` AS `pn2` ON
	( `pn1`.`users_id` = `pn2`.`users_id` ) AND
	( `pn1`.`phone` = `pn2`.`phone` ) AND
	( `pn1`.`id` < `pn2`.`id` )
WHERE
	( `pn2`.`id` IS null ) ;

ALTER TABLE `phone_numbers`
ADD UNIQUE( `users_id` , `phone` ) ;

-- возраст пользователя и количество номеров телефонов пользователя
DROP TEMPORARY TABLE IF EXISTS `t_users` ;
CREATE TEMPORARY TABLE IF NOT EXISTS `t_users`(
	`users_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор пользователя' ,
	`users_age` SMALLINT UNSIGNED NOT null COMMENT 'возраст' ,
	`phone_numbers_count` SMALLINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'количество номеров телефонов пользователя'
) COMMENT 'возраст пользователя и количество номеров телефонов пользователя' AS
SELECT
	`u1`.`id` AS `users_id` ,
	(
		( year( current_date( ) ) - year( `u1`.`birth_date` ) ) -
		cast( date_format( current_date( ) , '%m%d' ) < date_format( `u1`.`birth_date` , '%m%d' ) AS UNSIGNED )
	) AS `users_age` ,
	count( DISTINCT `pn1`.`phone` ) AS `phone_numbers_count`
FROM
	`users` AS `u1`

	LEFT OUTER JOIN `phone_numbers` AS `pn1` ON
	( `u1`.`id` = `pn1`.`users_id` )
WHERE
	( `u1`.`gender` = 3 )
GROUP BY
	1 , 2 ;

ALTER TABLE `t_users`
ADD INDEX( `users_id` , `users_age` ) ;

-- вывод результата
SELECT
	`u1`.`name` AS `users_name` ,
	`tu1`.`phone_numbers_count`
FROM
	`users` AS `u1`

	INNER JOIN `t_users` AS `tu1` ON
	( `u1`.`id` = `tu1`.`users_id` )
WHERE
	( `tu1`.`users_age` BETWEEN 18 AND 22 ) ;

DROP TEMPORARY TABLE IF EXISTS `t_users` ;