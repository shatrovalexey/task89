<?php
	/*
	* Задача №2
	* Имеется строка:
	* https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3
	* Напишите функцию, которая:
	* 1. удалит параметры со значением “3”;
	* 2. отсортирует параметры по значению;
	* 3. добавит параметр url со значением из переданной
	* ссылки без параметров (в примере: /test/index.html);
	* 4. сформирует и вернёт валидный URL на корень
	* указанного в ссылке хоста.
	* В указанном примере функцией должно быть
	* возвращено:
	* https://www.somehost.com/?param4=1&param3=2&param1=4&url=%2Ftest%2Findex.html
	*/

	/**
	* разбор URI на путь и HTTP-аргументы
	*
	* @param string $uri - URI
	*
	* @return array
	* - string $path - URI до HTTP-аргументов
	* - array $query - хэш-массив HTTP-аргументов
	*/
	function parse_uri( string $uri ):array {
		list( $path , $query ) = explode( '?' , $uri , 2 ) ;

		if ( $query ) {
			$query = parse_url( $uri , \PHP_URL_QUERY ) ;
			parse_str( $query , $query ) ;
		} else {
			$query = [ ] ;
		}

		return [ $path , $query , ] ;
	}

	/**
	* сборка URI из пути и HTTP-аргументов
	*
	* @param string $path - URI до HTTP-аргументов
	* @param array $query - хэш-массив HTTP-аргументов
	*
	* @return string - новый URI
	*/
	function unparse_uri( string $path , array $query = [ ] ):string {
		if ( empty( $query ) ) {
			return $path ;
		}

		$query = http_build_query( $query ) ;
		$result = implode( '?' , [ $path , $query , ] ) ;

		return $result ;
	}

	/**
	* фильтр URI
	*
	* @param string $uri - URI
	* @param function $routine - замыкание с аргументами:
	* - string $path - URI до HTTP-аргументов
	* - array $query - хэш-массив HTTP-аргументов
	*
	* @return string - новый URI
	*/
	function filter_uri( string $uri , closure $routine ):string {
		list( $path , $query ) = parse_uri( $uri ) ;

		$routine( $path , $query ) ;

		return unparse_uri( $path , $query ) ;
	}

	/**
	* 1. удалит параметры со значением “3”;
	*
	* @param string $uri - URI
	* @param string $value - значение
	*
	* @return string - новый URI
	*/
	function uri_delete_param_by_value( string $uri , string $value ):string {
		return filter_uri( $uri , function( string &$path , array &$query ) use( &$value ):void {
			foreach ( $query as $query_key => &$query_value ) {
				if ( $query_value != $value ) {
					continue ;
				}

				unset( $query[ $query_key ] ) ;
			}
		} ) ;
	}

	/**
	* 2. отсортирует параметры по значению;
	*
	* @param string $uri - URI
	*
	* @return string - новый URI
	*/
	function uri_order_param_by_value( string $uri ):string {
		return filter_uri( $uri , function( string &$path , array &$query ):void {
			ksort( $query ) ;
		} ) ;
	}

	/**
	* 3. добавит параметр url со значением из переданной ссылки без параметров (в примере: /test/index.html);
	*
	* @param string $uri - URI
	* @param string $key - ключ
	* @param string $value - значение
	*
	* @return string - новый URI
	*/
	function uri_add_param_key_value( string $uri , string $key , string $value ):string {
		return filter_uri( $uri , function( string &$path , array &$query ) use( &$key , &$value ):void {
			$query[ $key ] = $value ;
		} ) ;
	}

	/**
	* 4. сформирует и вернёт валидный URL на корень указанного в ссылке хоста.
	*
	* @param string $uri - URI
	*
	* @return string - новый URI
	*/
	function uri_base_path( string $uri ) {
		return filter_uri( $uri , function( string &$path , array &$query ):void {
			$query = [ ] ;
		} ) ;
	}

	/*
	* @var string $uri - входной URI
	*/
	$uri = 'https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3' ;

	foreach ( [ // Тестирование функций (хотя можно было бы организовать в класс)
		'1. удалит параметры со значением “3”' => function( $uri ) {
			return uri_delete_param_by_value( $uri , '3' ) ;
		} ,
		'2. отсортирует параметры по значению' => 'uri_order_param_by_value' ,
		'3. добавит параметр url со значением из переданной ссылки без параметров (в примере: /test/index.html);' => function( $uri ) {
			return uri_add_param_key_value( $uri , 'ключ' , 'значение' ) ;
		} ,
		'4. сформирует и вернёт валидный URL на корень указанного в ссылке хоста.' => 'uri_base_path' ,
	] as $title => $sub ) {
		echo $title . PHP_EOL . $sub( $uri ) . PHP_EOL ;
	}