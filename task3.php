<?php
	/**
	* Задача №3
	* Проведите рефакторинг, исправьте баги и
	* продокументируйте в стиле PHPDoc код, приведённый
	* ниже (таблица users здесь аналогична таблице users из
	* задачи №1).
	* Примечание: код написан исключительно в тестовых
	* целях, это не "жизненный пример" :)
	```
function load_users_data($user_ids) {
$user_ids = explode(',', $user_ids);
foreach ($user_ids as $user_id) {
$db = mysqli_connect("localhost", "root", "123123", "database");
$sql = mysqli_query($db, "SELECT * FROM users WHERE id=$user_id");
while($obj = $sql->fetch_object()){
$data[$user_id] = $obj->name;
}
mysqli_close($db);
}
return $data;
}
// Как правило, в $_GET['user_ids'] должна приходить строка
// с номерами пользователей через запятую, например: 1,2,17,48
$data = load_users_data($_GET['user_ids']);
foreach ($data as $user_id=>$name) {
echo "<a href=\"/show_user.php?id=$user_id\">$name</a>";
}
	```
	* Плюсом будет, если укажете, какие именно
	* уязвимости присутствуют в исходном варианте (если
	* таковые, на ваш взгляд, имеются), и приведёте
	* примеры их проявления.
	*/

	/**
	* Получить данные пользователя
	*
	* @param \PDO $dbh - указатель на объект для работы с СУБД
	* @param array $users_ids - идентификаторы пользователей
	*/
	function load_users_data( \PDO $dbh , array $users_ids = [ ] ) {
		if ( empty( $users_ids ) ) {
			return ;
		}

		$sth_users_sel = $dbh->prepare( "
SELECT
	`u1`.`id` ,
	`u1`.`name`
FROM
	`users` AS `u1`
WHERE
	( `u1`.`id` = :users_id ) ;
		" ) ;

		$users_id = current( $users_ids ) ;

		do {
			$sth_users_sel->execute( [
				':users_id' => $users_id ,
			] ) ;

			while ( $users = $sth_users_sel->fetch( \PDO::FETCH_ASSOC ) ) {
				yield $users ;
			}
		} while ( $users_id = next( $users_ids ) ) ;
	}

	// Как правило, в $_GET['user_ids'] должна приходить строка
	// с номерами пользователей через запятую, например: 1,2,17,48

	/**
	* @var \PDO $dbh - указатель на объект для работы с СУБД
	*/
	$dbh = new \PDO( 'mysql:dbname=database;host=localhost' , 'root' , '123123' ) ;
	$dbh->exec( 'SET NAMES utf8mb4' ) ;

	/**
	* @var array $users_ids - список идентификаторов пользователей
	*/
	$users_ids = explode( ',' , @$_GET[ 'user_ids' ] ) ;

	/**
	* @var \PDO $dbh - указатель на объект для работы с СУБД
	*/
	foreach ( get_users_data( $dbh , $users_ids ) as $users ) {
		?><a href="/show_user.php?id=<?=$users[ 'id' ]?>"><?=htmlspecialchars( $users[ 'name' ] )?></a><?php
	}

	$dbh->disconnect( ) ;

	/*
	* Плюсом будет, если укажете, какие именно
	* уязвимости присутствуют в исходном варианте (если
	* таковые, на ваш взгляд, имеются), и приведёте
	* примеры их проявления.
	*/
	/*
	* Бесполезные создание и разрыв подключений к СУБД в цикле;
	* SQL-injection;
	* не использование возможности подготовки запроса перед множественным его выполнением;
	* зазря выбираются все поля в SQL-запросе к `users`;
	* возможность инъекции в выходной HTML: "<a href=\"/show_user.php?id=$user_id\">$name</a>".
	*/